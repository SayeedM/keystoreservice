using System;
using Microsoft.Owin.Hosting;
using KeyStore.Services;

namespace KeyStore.Host
{
	public class MainClass
	{
		

		public static void Main (string[] args)
		{
            Type valuesControllerType = typeof(KeyStoreApiController);
			string baseUrl = "http://localhost:4444/";
			using (WebApp.Start<Startup>(baseUrl))
			{
				Console.WriteLine("Web Server is running.");
				Console.WriteLine("Press any key to quit.");
				Console.ReadLine();
			}
			Console.WriteLine ("Hello World!");
		}
	}
}
