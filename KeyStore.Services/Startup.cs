using System;
using Owin;
using System.Web.Http;
using System.Net;
using System.Net.Http.Formatting;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace KeyStore.Services
{
	public class Startup
	{

		public void Configuration(IAppBuilder app)
		{
			// Configure Web API for self-host. 
			var config = new HttpConfiguration();
            config.MapHttpAttributeRoutes ();

            config.Formatters.Clear();
            config.Formatters.Add(new JsonMediaTypeFormatter());
            config.Formatters.JsonFormatter.SerializerSettings =
                new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                };
			
            config.EnsureInitialized();
			app.UseWebApi(config); 

		}
	}

}

