﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeyStore.Services.Models
{
    public class LicenseEntry
    {
        public string ClientName { get; set; }
        public string Description { get; set; }
        public SoftwareEntry Software { get; set; }
        public string LicenseCode { get; set; }
        public string ClientKey { get; set; }
        public string PublicKey { get; set; }

        public LicenseEntry()
        {
            Software = new SoftwareEntry();
        }

    }
}
