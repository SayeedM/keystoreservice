﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeyStore.Services.Models
{
    public class SoftwareEntry
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public string Desc { get; set; }
        public string Url { get; set; }
        public string Repo { get; set; }
        public string PrivateKey { get; set; }
        public List<LicenseEntry> IssuedLicense { get; set; }

        public SoftwareEntry()
        {
            IssuedLicense = new List<LicenseEntry>();
        }
    }
}
