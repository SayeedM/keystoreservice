﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeyStore.Services.Models
{
    public class LicenseEntryBean
    {
        public string ClientId { get; set; }
        public string ClientKey { get; set; }
    }
}
