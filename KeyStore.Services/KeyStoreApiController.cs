using KeyStore.Services.Models;
using MongoDB;
using System;
using System.Web.Http;

namespace KeyStore.Services
{
	public class KeyStoreApiController : ApiController
	{
		public KeyStoreApiController ()
		{
		}

		/**
		 * Get Key given Client ID
		 */
		[HttpGet]
		[Route("api/fetchKey/{clientId}")]
		public LicenseEntryBean GetKey(string clientId){
            LicenseEntryBean bean = new LicenseEntryBean();
            bean.ClientId = clientId;
            bean.ClientKey = "Not Found";

            try
            {
                Mongo client = new Mongo();
                client.Connect();
                IMongoDatabase db = client.GetDatabase("LicenseDb");

                IMongoCollection<LicenseEntry> licenseCollection = db.GetCollection<LicenseEntry>();
                Document query = new Document { { "LicenseCode", clientId } };

                LicenseEntry entry = licenseCollection.FindOne(query);
                
                bean.ClientId = clientId;
                bean.ClientKey = entry.ClientKey;

                return bean;

            }
            catch (Exception ex)
            {
                return bean;
            }
            

            return bean;
		}
	}
}

